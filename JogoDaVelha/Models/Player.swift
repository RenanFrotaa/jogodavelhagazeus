//
//  Player.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 19/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import Foundation

struct Player {
    
    var name: String = ""
    var isNPC: Bool = false
    var playerNumber: Int = 0
    var victories: Int = 0
    var losses: Int = 0
    var draws: Int = 0
    var points: Int = 0
    
    init?(name: String, playerNumber: Int) {
        if name == "" {
            self.name = "CPU"
            self.isNPC = true
        } else {
            self.name = name
            self.isNPC = false
        }
        
        self.playerNumber = playerNumber
    }
}

extension Player {
    
    static func setArrayOfPlayersWithNames(names: [String]) -> [Player] {
        var players: [Player] = []
        for (index, element) in names.enumerated() {
            let player = Player(name: element, playerNumber: index + 1)
            players.append(player!)
        }
        
        return players
    }
}
