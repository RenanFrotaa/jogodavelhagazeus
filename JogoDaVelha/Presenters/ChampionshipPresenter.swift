//
//  ChampionshipPresenter.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 20/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

protocol ChampionshipPresenterProtocol {
    func showChampionAlert(alert: UIAlertController)
    func backToMenu()
}


class ChampionshipPresenter: NSObject {
    
    // MARK: VARIABLES
    var delegate : ChampionshipPresenterProtocol?

    // MARK: FUNCTIONS
    func getAllMatches(players: [Player]) -> [[Player]] {
        let allCombinations = players.combinationsWithoutRepetition
        var matchesCombinations: [[Player]] = []
        for combination in allCombinations {
            if combination.count == 2 {
                matchesCombinations.append(combination)
            }
        }
        
        return matchesCombinations
    }
    
    func createChampionAlert(champion: String) {
        let alert: UIAlertController = UIAlertController(title: "Campeão!", message: "Parabéns \(champion), você(s) é(ão) o(s) grande(s) campeão(ões)!", preferredStyle: .alert)
    
        alert.addAction(UIAlertAction(title: "Voltar ao menu", style: .default, handler: { action in
            self.backToMenu()
        }))
        
        alert.addAction(UIAlertAction(title: "Ver tabela", style: .default, handler: nil))
        
        self.delegate?.showChampionAlert(alert: alert)
    }
    
    func backToMenu() {
        self.delegate?.backToMenu()
    }
}
