//
//  CasualGameOptionsPresenter.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 19/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

protocol GameOptionsPresenterProtocol {
    func showAlertPlayersName(alert: UIAlertController)
    func showGame(playersName: [String], numberOfPlayers: Int)
}

class GameOptionsPresenter: NSObject {
    
    // MARK: VARIABLES
    var delegate : GameOptionsPresenterProtocol?
    
    // MARK: FUNCTIONS
    func optionChoosed(numberOfPlayers: Int) {
        
        let alert: UIAlertController = UIAlertController().buildPlayerNamesAlert(numberOfPlayers: numberOfPlayers)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { action in
            var playersName: [String] = []
            for i in 0..<numberOfPlayers {
                if alert.textFields![i].text! == ""{
                    playersName.append("Player \(i+1)")
                } else {
                    playersName.append(alert.textFields![i].text!)
                }
            }
            
            self.delegate?.showGame(playersName: playersName, numberOfPlayers: numberOfPlayers)
        }))
        
        self.delegate?.showAlertPlayersName(alert: alert)
    }
}
