//
//  GamePresenter.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 19/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

protocol GamePresenterProtocol {
    func showPlayerWins(playerWinner: Int)
    func incrementPlayerVictories(playerNumber: Int)
    func showDraw()
    func setupViewForNewTurn(currentyPlaying: Int)
    func setupViewStats()
    func setupInitialTurnIndicatorLabel()
    func setupViewToNewGame(currentlyPlaying: Int)
    func backToMenu()
    func backToChampionshipTable(isDraw: Bool, currentlyPlaying: Int)
    func getButtonWithTag(tag: Int) -> UIButton
}

class GamePresenter: NSObject {
    
    // MARK: VARIABLES
    var delegate : GamePresenterProtocol?
    var isChampionshipGame: Bool = false
    var isDraw: Bool = false
    var player1PointsChecked: [Int] = []
    var player2PointsChecked: [Int] = []
    var currentlyPlaying: Int = 1
    var winnersPointsCombinations: [[Int]] = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]]
    
    // MARK: FUNCTIONS
    func setupViewDidLoad() {
        self.delegate?.setupViewStats()
        self.delegate?.setupInitialTurnIndicatorLabel()
    }
    
    func makePlay(sender: UIButton) {
        if currentlyPlaying == 1 {
            sender.setBackgroundImage(UIImage(named: "\(Configurations.getCurrentlySelectorsName())_1"), for: .normal)
            player1PointsChecked.append(sender.tag)
            sender.isUserInteractionEnabled = false
            self.checkWinner(playerPoints: player1PointsChecked)
            
        } else {
            sender.setBackgroundImage(UIImage(named: "\(Configurations.getCurrentlySelectorsName())_2"), for: .normal)
            sender.isUserInteractionEnabled = false
            player2PointsChecked.append(sender.tag)
            self.checkWinner(playerPoints: player2PointsChecked)
        }
    }

    func checkWinner(playerPoints: [Int]) {
        for pointsCombination in winnersPointsCombinations {
            if playerPoints.contains(array: pointsCombination) {
                self.player1PointsChecked = []
                self.player2PointsChecked = []
                self.delegate?.incrementPlayerVictories(playerNumber: currentlyPlaying)
                self.isDraw = false
                self.delegate?.showPlayerWins(playerWinner: currentlyPlaying)
                return
            }
        }
        
        if player1PointsChecked.count + player2PointsChecked.count == 9 {
            self.player1PointsChecked = []
            self.player2PointsChecked = []
            self.isDraw = true
            self.delegate?.showDraw()
        }
        
        self.changeCurrentlyPlayer()
    }
    
    func changeCurrentlyPlayer() {
        if currentlyPlaying == 1 {
            currentlyPlaying = 2
        } else {
            currentlyPlaying = 1
        }
        
        self.delegate?.setupViewForNewTurn(currentyPlaying: currentlyPlaying)
    }
    
    func makeNPCPlay() {
        let arrayAllPoints: [Int] = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        let availablePoints = arrayAllPoints.difference(from: player1PointsChecked + player2PointsChecked)
        let button = self.delegate?.getButtonWithTag(tag: availablePoints.randomItem()!)
        UIApplication.shared.beginIgnoringInteractionEvents()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.makePlay(sender: button!)
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    func createFinishedGameAlert(title: String, message: String) -> UIAlertController {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if isChampionshipGame {
            alert.addAction(UIAlertAction(title: "Voltar a tabela", style: .default, handler: { action in
                self.backToChampionshipTable()
            }))

        } else {
            alert.addAction(UIAlertAction(title: "Voltar ao menu", style: .default, handler: { action in
                self.backToMenu()
            }))
            
            alert.addAction(UIAlertAction(title: "Jogar de novo", style: .default, handler: { action in
                self.playAgain()
            }))
        }
        
        return alert
    }
    
    func createFinishChampionshipGameAlert(message: String) -> UIAlertController {
        let alert: UIAlertController = UIAlertController(title: "Parabéns", message: message, preferredStyle: .alert)

        return alert
    }
    
    func playAgain() {
        self.delegate?.setupViewToNewGame(currentlyPlaying: currentlyPlaying)
    }
    
    func backToMenu() {
        self.delegate?.backToMenu()
    }
    
    func backToChampionshipTable() {
        self.delegate?.backToChampionshipTable(isDraw: isDraw, currentlyPlaying: currentlyPlaying)
    }
}
