//
//  ChampionshipTableViewCell.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 20/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

class ChampionshipTableViewCell: UITableViewCell {
    
    // MARK: - OUTLETS
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var playerPointsLabel: UILabel!
    @IBOutlet weak var playerMatchesLabel: UILabel!
    @IBOutlet weak var playerVictoriesLabel: UILabel!
    @IBOutlet weak var playerDrawsLabel: UILabel!
    @IBOutlet weak var playerDefeatsLabel: UILabel!
    var player: Player = Player(name: "", playerNumber: 0)! { didSet { setupPlayer() } }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//    }
    
    func setupPlayer() {
        self.playerNameLabel.text = player.name
        self.playerPointsLabel.text = "\(player.points)"
        self.playerMatchesLabel.text = "\(player.draws + player.victories + player.losses)"
        self.playerVictoriesLabel.text = "\(player.victories)"
        self.playerDrawsLabel.text = "\(player.draws)"
        self.playerDefeatsLabel.text = "\(player.losses)"
    }
    
}
