//
//  ConfigurationTableViewController.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 21/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

class ConfigurationTableViewController: UITableViewController {

    // MARK: - LYFECICLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.title = "Configurações"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
}
