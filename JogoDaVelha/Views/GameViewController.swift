//
//  GameViewController.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 19/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

protocol GameProtocol {
    func finishMatch(isDraw: Bool, currentlyPlaying: Int)
}


class GameViewController: UIViewController {
    
    // MARK: - OUTLETS
    @IBOutlet var gameButtons: [UIButton]!
    @IBOutlet weak var player1NameLabel: UILabel!
    @IBOutlet weak var player1VictoriesLabel: UILabel!
    @IBOutlet weak var player2NameLabel: UILabel!
    @IBOutlet weak var player2VictoriesLabel: UILabel!
    @IBOutlet weak var turnIndicatorLabel: UILabel!
    
    // MARK: - VARIABLES
    var delegate : GameProtocol?
    var player1: Player? = nil
    var player2: Player? = nil
    let eventHandler = GamePresenter()
    var isChampionshipGame: Bool = false
    

    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventHandler.delegate = self
        self.eventHandler.isChampionshipGame = self.isChampionshipGame
        self.eventHandler.setupViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Partida"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    // MARK: - ACTIONS
    @IBAction func makePlayButtonPressed(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        self.eventHandler.makePlay(sender: button)
    }
    
    // MARK: - FUNCTIONS
    func returnPlayer(playerNumber: Int) -> Player {
        if playerNumber == 1 {
            return player1!
        }
        
        return player2!
    }
}

extension GameViewController: GamePresenterProtocol {
    func showPlayerWins(playerWinner: Int) {
        
        self.setupViewStats()
        let alert = self.eventHandler.createFinishedGameAlert(title: "Parabéns!", message: "\(returnPlayer(playerNumber: playerWinner).name) venceu!")
        self.present(alert, animated: true, completion: nil)
    }
    
    func showDraw() {
        let alert = self.eventHandler.createFinishedGameAlert(title: "Tente de novo!", message: "Vocês empataram!")
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func incrementPlayerVictories(playerNumber: Int) {
        if playerNumber == 1 {
            player1!.victories += 1
        } else {
            player2!.victories += 1
        }
    }
    
    func setupViewForNewTurn(currentyPlaying: Int) {
        turnIndicatorLabel.text = "Turno: \(returnPlayer(playerNumber: currentyPlaying).name)"
        if returnPlayer(playerNumber: currentyPlaying).isNPC {
            self.eventHandler.makeNPCPlay()
        }
    }
    
    func setupViewStats() {
        self.player1NameLabel.text = player1?.name
        self.player1VictoriesLabel.text = "Vitórias: \(player1!.victories)"
        self.player2NameLabel.text = player2?.name
        self.player2VictoriesLabel.text = "Vitórias: \(player2!.victories)"
    }
    
    func setupInitialTurnIndicatorLabel() {
        self.turnIndicatorLabel.text = "Turno: \(player1!.name)"
    }
    
    func setupViewToNewGame(currentlyPlaying: Int) {
        for button in gameButtons {
            button.setBackgroundImage(nil, for: .normal)
            button.isUserInteractionEnabled = true
        }
        
        let player = returnPlayer(playerNumber: currentlyPlaying)
        if player.isNPC {
            self.eventHandler.makeNPCPlay()
        }
    }
    
    func getButtonWithTag(tag: Int) -> UIButton {
        for button in gameButtons {
            if button.tag == tag {
                return button
            }
        }
        
        return UIButton()
    }
    
    func backToMenu() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func backToChampionshipTable(isDraw: Bool, currentlyPlaying: Int) {
        self.navigationController?.popViewController(animated: true)
        self.delegate?.finishMatch(isDraw: isDraw, currentlyPlaying: returnPlayer(playerNumber: currentlyPlaying).playerNumber)
    }
}
