//
//  SelectorOptionsTableViewController.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 20/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

class SelectorOptionsTableViewController: UITableViewController {
    
    // MARK: - VARIABLES
    let selectorsArray: [[String : Any]] = Configurations.getSelectorsOptions()

    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Opções de seletor"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }


    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectorsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        cell?.backgroundColor = .clear
        cell?.selectionStyle = .none
        cell?.tintColor = .white
        cell?.textLabel?.font = cell?.textLabel?.font.withSize(20)
        cell?.textLabel?.textColor = .white
        cell?.textLabel?.text = selectorsArray[indexPath.row]["title"] as? String ?? ""
        if selectorsArray[indexPath.row]["reference"] as? String ?? "" == Configurations.getCurrentlySelectorsName() {
            cell?.accessoryType = .checkmark
        } else {
            cell?.accessoryType = .none
        }

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Configurations.setCurrentlySelectorsName(name: selectorsArray[indexPath.row]["reference"] as? String ?? "")
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
