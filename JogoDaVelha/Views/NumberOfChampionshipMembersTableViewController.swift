//
//  NumberOfChampionshipMembersTableViewController.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 20/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

class NumberOfChampionshipMembersTableViewController: UITableViewController {

    // MARK: - VARIABLES
    let eventHandler = GameOptionsPresenter()
    
    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventHandler.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Número de participantes"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }


    // MARK: - TABLEVIEW DELEGATE
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.eventHandler.optionChoosed(numberOfPlayers: indexPath.row + 3)
    }
}

extension NumberOfChampionshipMembersTableViewController: GameOptionsPresenterProtocol {
    func showAlertPlayersName(alert: UIAlertController) {
        self.present(alert, animated: true, completion: nil)
    }
    
    func showGame(playersName: [String], numberOfPlayers: Int) {
        let championshipTableViewController = Constants.storyboard.instantiateViewController(withIdentifier: "ChampionshipTableViewController") as! ChampionshipTableViewController
        championshipTableViewController.players = Player.setArrayOfPlayersWithNames(names: playersName)
        
        self.navigationController?.pushViewController(championshipTableViewController, animated: true)

    }    
}
