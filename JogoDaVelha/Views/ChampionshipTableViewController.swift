//
//  ChampionshipTableViewController.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 20/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

class ChampionshipTableViewController: UITableViewController {
    
    // MARK: - OUTLETS
    @IBOutlet weak var nextMatchButton: UIButton!
    
    // MARK: - VARIABLES
    var players: [Player] = []
    let eventHandler = ChampionshipPresenter()
    var arrayMatches: [[Player]] = []

    // MARK: - LYFECICLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventHandler.delegate = self
        tableView.register(UINib(nibName: "ChampionshipTableViewCell", bundle: nil), forCellReuseIdentifier: "ChampionshipTableViewCell")
        arrayMatches = self.eventHandler.getAllMatches(players: players)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Tabela"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }

    
    // MARK: - FUNCIONTS
    func getChampionsName() -> String {
        var championsName: String = players[0].name
        for (index, element) in players.enumerated() {
            if element.points == players[0].points && index != 0 {
                championsName += ", \(element.name)"
            }
        }
        
        return championsName
    }
    
    // MARK: - ACTIONS
    @IBAction func nextMatchButtonPressed(_ sender: Any) {
        let gameViewController = Constants.storyboard.instantiateViewController(withIdentifier: "GameViewController") as! GameViewController
        gameViewController.delegate = self
        gameViewController.player1 = arrayMatches[0][0]
        gameViewController.player2 = arrayMatches[0][1]
        gameViewController.isChampionshipGame = true
        
        self.navigationController?.pushViewController(gameViewController, animated: true)
    }
    // MARK: - TABLEVIEW DATA SOURCE
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChampionshipTableViewCell", for: indexPath)
            as! ChampionshipTableViewCell
        cell.player = players[indexPath.row]
        
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension ChampionshipTableViewController: GameProtocol {
    func finishMatch(isDraw: Bool, currentlyPlaying: Int) {
            for (index, element) in players.enumerated() {
                if element.playerNumber == arrayMatches[0][0].playerNumber || element.playerNumber == arrayMatches[0][1].playerNumber {
                    var updatedPlayer = element
                    if isDraw {
                        updatedPlayer.draws += 1
                        updatedPlayer.points += 1
                    } else {
                        if element.playerNumber == currentlyPlaying {
                            updatedPlayer.victories += 1
                            updatedPlayer.points += 3
                        } else {
                            updatedPlayer.losses += 1
                        }
                    }
                    players[index] = updatedPlayer
                }
            }
        
        players = players.sorted(by: { $0.points > $1.points })
        arrayMatches.remove(at: 0)
        if arrayMatches.count == 0 { self.eventHandler.createChampionAlert(champion: getChampionsName()) }
        tableView.reloadData()
    }
}

extension ChampionshipTableViewController: ChampionshipPresenterProtocol {
    func showChampionAlert(alert: UIAlertController) {
        self.nextMatchButton.isUserInteractionEnabled = false
        self.present(alert, animated: true, completion: nil)
    }
    
    func backToMenu() {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
