//
//  StartTableViewController.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 19/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

class StartTableViewController: UITableViewController {

    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Início"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
}
