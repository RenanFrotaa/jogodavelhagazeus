//
//  CasualGameOptionsTableViewController.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 19/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

class CasualGameOptionsTableViewController: UITableViewController {

    // MARK: - VARIABLES
    let eventHandler = GameOptionsPresenter()

    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventHandler.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Opções de jogo"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }

    
    // MARK: - TABLEVIEW DELEGATE
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.eventHandler.optionChoosed(numberOfPlayers: indexPath.row + 1)
    }
}

extension CasualGameOptionsTableViewController: GameOptionsPresenterProtocol {
    func showAlertPlayersName(alert: UIAlertController) {
        self.present(alert, animated: true, completion: nil)
    }
    
    func showGame(playersName: [String], numberOfPlayers: Int) {
        let gameViewController = Constants.storyboard.instantiateViewController(withIdentifier: "GameViewController") as! GameViewController
        
        gameViewController.player1 = Player(name: playersName[0], playerNumber: 1)
        gameViewController.player2 = Player(name: (playersName.indices.contains(1) ? playersName[1] : ""), playerNumber: 2)

        self.navigationController?.pushViewController(gameViewController, animated: true)
    }
}
