//
//  Constants.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 19/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

struct Constants {
    static let storyboard = UIStoryboard(name: "Main", bundle: nil)
}
