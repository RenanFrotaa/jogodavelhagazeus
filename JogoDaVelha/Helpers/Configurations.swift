//
//  Configurations.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 19/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

class Configurations {

    static func getCurrentlySelectorsName() -> String {
        let selectorsName: String = UserDefaults().string(forKey: "checkersName") ?? "default"
        
        return selectorsName
    }
    
    static func setCurrentlySelectorsName(name: String) {
        UserDefaults().set(name, forKey: "checkersName")
    }
    
    static func getSelectorsOptions() -> [[String : Any]] {
        let configurationDict: [String : Any] = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "Configuration", ofType: "plist")!) as! [String : Any]
        let selectorsConfig: [String : Any] = configurationDict["Selectors"] as? [String : Any] ?? [:]
        
        return selectorsConfig["Options"] as? [[String : Any]] ?? []
    }
}
