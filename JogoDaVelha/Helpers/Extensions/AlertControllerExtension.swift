//
//  AlertControllerExtension.swift
//  JogoDaVelha
//
//  Created by Renan dos Santos Frota on 20/09/2018.
//  Copyright © 2018 RsfApps. All rights reserved.
//

import UIKit

extension UIAlertController {
    func buildPlayerNamesAlert(numberOfPlayers: Int) -> UIAlertController {
        let alert = UIAlertController(title: "Por favor, digite o nome(s) do(s) jogador(es)", message: "", preferredStyle: .alert)
        
        for i in 1...numberOfPlayers {
            alert.addTextField { (textField) in
                textField.placeholder = "Digite o nome do jogador \(i)"
            }
        }
        
        return alert
    }
}
