
"Jogo da Velha" é um jogo básico de tic tac toe posibilitando o usuário jogar partidas com amigos ou contra o próprio celular, em jogos casuais ou em um mini-campeonato.

- Funcionalidades:
* Jogo Casual - Jogue com um amigo ou contra o próprio celular em partida única;
*  Campeonato - Jogue em um torneio de 3, 4 ou 5 pessoas, no estilo todo contra todos, o maior pontuador será o campeão;
*  Configurações - Escolha o seletor que será usado no seu jogo;

- Linguagens:
* Swift;

- Arquitetura:
* MVP;
